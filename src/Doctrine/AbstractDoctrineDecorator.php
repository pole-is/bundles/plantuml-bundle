<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataFactory;
use Irstea\PlantUmlBundle\Model\DecoratorInterface;
use ReflectionClass;

/**
 * Description of AbstractDoctrineDecorator.
 */
abstract class AbstractDoctrineDecorator implements DecoratorInterface
{
    /**
     * @var ClassMetadataFactory
     */
    protected $metadataFactory;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->metadataFactory = $manager->getMetadataFactory();
    }

    /**
     * @param callable        $callback
     * @param ReflectionClass $class
     *
     * @return mixed
     */
    protected function withMetadata($callback, ReflectionClass $class = null)
    {
        if (!$class) {
            return null;
        }

        $className = $class->getName();
        if (!$this->metadataFactory->hasMetadataFor($className)) {
            return null;
        }

        return $callback($this->metadataFactory->getMetadataFor($className));
    }
}
