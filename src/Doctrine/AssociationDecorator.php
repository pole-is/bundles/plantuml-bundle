<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata;
use Irstea\PlantUmlBundle\Model\Arrow\BaseArrow;
use Irstea\PlantUmlBundle\Model\ClassVisitorInterface;
use Irstea\PlantUmlBundle\Model\Node\Member\Member;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of RelationDecorator.
 */
class AssociationDecorator extends AbstractDoctrineDecorator
{
    use \Irstea\PlantUmlBundle\Model\Decorator\InheritableItemDecoratorTrait;

    protected function extractItems(ReflectionClass $class)
    {
        return $this->withMetadata(
            function ($metadata) {
                return $metadata->getAssociationMappings();
            },
            $class
        );
    }

    protected function decorateItem(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor, $association): void
    {
        if (!$association['isOwningSide']) {
            return;
        }

        $target = $visitor->visitClass($association['targetEntity']);
        if ($target === false) {
            $type = $association['targetEntity'];
            if ($association['type'] & ClassMetadata::TO_MANY != 0) {
                $type .= '[]';
            }
            $node->addAttribute(new Member($association['fieldName'], $type));

            return;
        }

        $linkSource = '';
        $linkTarget = '>';
        if ($association['isCascadeRemove']) {
            $linkSource = 'o';
        }

        $sourceCardinality = '';
        $targetCardinality = '';
        switch ($association['type']) {
            case ClassMetadata::ONE_TO_ONE:
                $sourceCardinality = '1';
                $targetCardinality = '1';
                break;
            case ClassMetadata::ONE_TO_MANY:
                $sourceCardinality = '1';
                $targetCardinality = '*';
                break;
            case ClassMetadata::MANY_TO_MANY:
                $sourceCardinality = '*';
                $targetCardinality = '*';
                break;
            case ClassMetadata::MANY_TO_ONE:
                $sourceCardinality = '*';
                $targetCardinality = '1';
                break;
        }

        $node->addArrow(
            new BaseArrow(
                $node,
                $target,
                '--',
                $association['fieldName'] . ' >',
                $linkSource,
                $linkTarget,
                $sourceCardinality,
                $targetCardinality
            )
        );
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'associations';
    }
}
