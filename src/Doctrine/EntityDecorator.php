<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Doctrine;

use Irstea\PlantUmlBundle\Model\ClassVisitorInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;

/**
 * Description of EntityDecorator.
 */
class EntityDecorator extends AbstractDoctrineDecorator
{
    public function decorate(\ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        $this->withMetadata(
            function ($metadata) use ($node): void {
                if ($metadata->isMappedSuperclass) {
                    $node->addStereotype('mappedSuperClass');
                } elseif ($metadata->isEmbeddedClass) {
                    $node->addStereotype('embedded');
                } else {
                    $node->addStereotype('entity');
                }
            },
            $class
        );
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'entity';
    }
}
