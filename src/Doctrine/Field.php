<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Doctrine;

use Irstea\PlantUmlBundle\Model\Node\Member\Member;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Field.
 */
class Field extends Member
{
    /**
     * @var bool
     */
    private $isUnique;

    /**
     * @var bool
     */
    private $isNullable;

    /**
     * @var bool
     */
    private $isIdentifier;

    public function __construct($symbol, $type, $isUnique = false, $isNullable = false, $isIdentifier = false)
    {
        parent::__construct($symbol, $type);
        $this->isUnique = $isUnique;
        $this->isNullable = $isNullable;
        $this->isIdentifier = $isIdentifier;
    }

    protected function writeSymbolTo(WriterInterface $writer)
    {
        if ($this->isIdentifier) {
            $writer->write('<b>');
        }
        if (!$this->isNullable) {
            $writer->write('<i>');
        }
        if ($this->isUnique) {
            $writer->write('<u>');
        }
        parent::writeSymbolTo($writer);
        if ($this->isUnique) {
            $writer->write('</u>');
        }
        if (!$this->isNullable) {
            $writer->write('</i>');
        }
        if ($this->isIdentifier) {
            $writer->write('</b>');
        }

        return $this;
    }
}
