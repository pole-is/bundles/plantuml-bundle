<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Doctrine;

/**
 * Description of DoctrineNamespace.
 */
class DoctrineNamespace extends \Irstea\PlantUmlBundle\Model\Namespace_\MappedNamespace
{
    public const CONF_TYPE = 'entities';
    public const SEPARATOR = '::';

    public function __construct(\Doctrine\ORM\EntityManagerInterface $em)
    {
        $entityNamespaces = $em->getConfiguration()->getEntityNamespaces();

        $mapping = [];
        foreach ($entityNamespaces as $alias => $namespace) {
            $mapping[$namespace . '\\'] = $alias . '::';
        }
        parent::__construct($mapping);
    }
}
