<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata;
use Irstea\PlantUmlBundle\Model\ClassVisitorInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of RelationDecorator.
 */
class FieldDecorator extends AbstractDoctrineDecorator
{
    use \Irstea\PlantUmlBundle\Model\Decorator\InheritableItemDecoratorTrait;

    protected function extractItems(ReflectionClass $class)
    {
        return $this->withMetadata(
            function (ClassMetadata $metadata) {
                /* @var $metadata \Doctrine\ORM\Mapping\ClassMetadata */
                return $metadata->fieldMappings;
            },
            $class
        );
    }

    protected function decorateItem(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor, $field): void
    {
        $isIdentifier = $this->withMetadata(
            function (ClassMetadata $metadata) use ($field) {
                /* @var $metadata \Doctrine\ORM\Mapping\ClassMetadata */
                return $metadata->isIdentifier($field['fieldName']);
            },
            $class
        );
        $node->addAttribute(
            new Field(
                $field['fieldName'],
                $field['type'],
                $field['unique'],
                $field['nullable'],
                $isIdentifier
            )
        );
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'fields';
    }
}
