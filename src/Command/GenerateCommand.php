<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Command;

use Irstea\PlantUmlBundle\Writer\OutputWriter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Exception\InvalidArgumentException;

/**
 * Description of ImportAffiliationCommand.
 */
class GenerateCommand extends ContainerAwareCommand
{
    protected function configure(): void
    {
        $this
            ->setName('irstea:plantuml:generate')
            ->setDescription('Génère un graphe en PlantUML.')
            ->addArgument('graph', InputArgument::REQUIRED, 'Nom du graphe à générer');
    }

    /**
     * @SuppressWarnings(UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $name = $input->getArgument('graph');
        $serviceId = "irstea_plant_uml.graph.$name";

        if (!$this->getContainer()->has($serviceId)) {
            throw new InvalidArgumentException("Le graphe '$name' n'est pas défini.");
        }

        $writer = new OutputWriter($output);
        $graph = $this->getContainer()->get($serviceId);
        $graph->visitAll();
        $graph->writeTo($writer);
    }
}
