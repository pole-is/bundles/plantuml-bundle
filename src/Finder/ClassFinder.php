<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Finder;

use ArrayIterator;
use Irstea\PlantUmlBundle\Model\Filter\DirectoryFilter;
use ReflectionClass;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Description of ClassFinder.
 */
class ClassFinder implements FinderInterface
{
    /**
     * @var string[]
     */
    private $directories;

    /**
     * @var \ReflectionClass
     */
    private $classes = null;

    /**
     * ClassFinder constructor.
     *
     * @param string[] $directories
     */
    public function __construct(array $directories)
    {
        $this->directories = $directories;
    }

    private function initialize(): void
    {
        $files = Finder::create()
            ->in($this->directories)
            ->files()
            ->name('*.php')
            ->getIterator();

        foreach ($files as $file) {
            /* @var $file SplFileInfo */
            $path = $file->getPathname();
            try {
                irstea_plantmul_include($path);
            } catch (Exception $ex) {
                printf("%s: %s (%s)\n", $path, \get_class($ex), $ex->getMessage());
            }
        }

        $filter = new DirectoryFilter($this->directories);
        $this->classes = [];
        foreach (get_declared_classes() as $className) {
            $class = new ReflectionClass($className);
            if ($filter->accept($class)) {
                $this->classes[$className] = $class;
            }
        }
    }

    /**
     * @return ArrayIterator|\Traversable
     */
    public function getIterator()
    {
        if ($this->classes === null) {
            $this->initialize();
        }

        return new ArrayIterator($this->classes);
    }

    public function toConfig(array &$conf): void
    {
        $conf['type'] = 'classes';
        $conf['directories'] = $this->directories;
    }
}
