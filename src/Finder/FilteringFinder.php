<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Finder;

use CallbackFilterIterator;
use Irstea\PlantUmlBundle\Model\ClassFilterInterface;

/**
 * Description of FilteringFinder.
 */
class FilteringFinder implements FinderInterface
{
    /**
     * @var FinderInterface
     */
    private $inner;

    /**
     * @var ClassFilterInterface
     */
    private $filter;

    public function __construct(FinderInterface $inner, ClassFilterInterface $filter)
    {
        $this->inner = $inner;
        $this->filter = $filter;
    }

    public function getIterator()
    {
        return new CallbackFilterIterator($this->inner->getIterator(), [$this->filter, 'accept']);
    }

    public function toConfig(array &$conf): void
    {
        $this->inner->toConfig($conf);
        $this->filter->toConfig($conf);
    }
}
