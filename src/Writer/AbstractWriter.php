<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Writer;

/**
 * Description of AbstractWriter.
 */
abstract class AbstractWriter implements WriterInterface
{
    /**
     * @var int
     */
    private $indentation = 0;

    /**
     * @var string
     */
    private $buffer;

    public function dedent($n = 1)
    {
        $this->indentation = max(0, $this->indentation - $n);

        return $this;
    }

    public function indent($n = 1)
    {
        $this->indentation += $n;

        return $this;
    }

    public function writeFormatted($format)
    {
        $args = \func_get_args();
        array_shift($args);
        $this->write(vsprintf($format, $args));

        return $this;
    }

    public function write($data)
    {
        $buffer = $this->buffer . $data;
        $indentation = str_repeat('  ', $this->indentation);
        $current = 0;
        while ($current < \strlen($buffer) && false !== ($next = strpos($buffer, "\n", $current))) {
            ++$next;
            $this->doWrite($indentation . substr($buffer, $current, $next - $current));
            $current = $next;
        }
        $this->buffer = substr($buffer, $current);

        return $this;
    }

    /**
     * @param string $data
     */
    abstract protected function doWrite($data);
}
