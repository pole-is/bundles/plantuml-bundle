<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model;

use Irstea\PlantUmlBundle\Writer\WritableInterface;

interface NamespaceInterface extends WritableInterface, ToConfigInterface
{
    /**
     * @param string $namespaceName
     *
     * @return NamespaceInterface
     */
    public function getNamespace($namespaceName);

    /**
     * @param string $className
     *
     * @return string
     */
    public function getNodeId($className);

    /**
     * @param string $className
     *
     * @return string
     */
    public function getNodeLabel($className);

    /**
     * @return self
     */
    public function addNode(NodeInterface $node);

    /**
     * @return self
     */
    public function addArrow(ArrowInterface $arrow);
}
