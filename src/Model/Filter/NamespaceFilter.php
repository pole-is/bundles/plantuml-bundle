<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Filter;

use ReflectionClass;

/**
 * Description of DirectoryFilter.
 */
class NamespaceFilter extends AbstractListFilter
{
    public const CONF_TYPE = 'namespaces';

    /**
     * {@inheritdoc}
     */
    protected function extract(ReflectionClass $class): string
    {
        return $class->getNamespaceName();
    }

    /**
     * {@inheritdoc}
     */
    protected function matches($tested, $reference): bool
    {
        return strpos($tested, $reference) === 0;
    }

    /**
     * {@inheritdoc}
     */
    protected function normalize($namespace): string
    {
        return trim($namespace, '\\') . '\\';
    }
}
