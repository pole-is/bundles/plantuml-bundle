<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Filter;

use Irstea\PlantUmlBundle\Model\ClassFilterInterface;
use ReflectionClass;

/**
 * Description of AbstractListFilter.
 */
abstract class AbstractListFilter implements ClassFilterInterface
{
    /**
     * string.
     */
    protected const CONF_TYPE = 'UNDEFINED';

    /**
     * @var string[]
     */
    private $allowed = [];

    /**
     * @var bool
     */
    private $notFound;

    /**
     * AbstractListFilter constructor.
     *
     * @param bool $notFound
     */
    public function __construct(array $allowed, $notFound = false)
    {
        $this->allowed = array_map([$this, 'normalize'], $allowed);
        $this->notFound = $notFound;
    }

    /**
     * @return bool
     */
    public function accept(ReflectionClass $class)
    {
        $tested = $this->normalize($this->extract($class));
        foreach ($this->allowed as $reference) {
            if ($this->matches($tested, $reference)) {
                return !$this->notFound;
            }
        }

        return $this->notFound;
    }

    public function toConfig(array &$conf): void
    {
        $key = $this->notFound ? 'exclude' : 'include';
        if (!\array_key_exists($key, $conf)) {
            $conf[$key] = [];
        }
        $conf[$key][static::CONF_TYPE] = $this->allowed;
    }

    /**
     * @param string $value
     */
    abstract protected function normalize($value): string;

    abstract protected function extract(ReflectionClass $class): string;

    /**
     * @param mixed $tested
     * @param mixed $reference
     */
    abstract protected function matches($tested, $reference): bool;
}
