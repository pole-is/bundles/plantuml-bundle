<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Arrow;

use Irstea\PlantUmlBundle\Model\ArrowInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Arrow.
 */
class BaseArrow implements ArrowInterface
{
    /**
     * @var NodeInterface
     */
    private $source;

    /**
     * @var NodeInterface
     */
    private $target;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $linkSource;

    /**
     * @var string
     */
    private $linkTarget;

    /**
     * @var string
     */
    private $sourceCardinality;

    /**
     * @var string
     */
    private $targetCardinality;

    /**
     * @var mixed|null
     */
    private $label;

    public function __construct(
        NodeInterface $source,
        NodeInterface $target,
        $link = '--',
        $label = null,
        $linkSource = '',
        $linkTarget = '',
        $sourceCardinality = '',
        $targetCardinality = ''
    ) {
        $this->source = $source;
        $this->target = $target;
        $this->link = $link;
        $this->label = $label;
        $this->linkSource = $linkSource;
        $this->linkTarget = $linkTarget;
        $this->sourceCardinality = $sourceCardinality;
        $this->targetCardinality = $targetCardinality;
    }

    public function writeTo(WriterInterface $writer)
    {
        $this->source->writeAliasTo($writer);
        $this->writeLinkTo($writer);
        $this->target->writeAliasTo($writer);
        $this->writeLabelTo($writer);
        $writer->write("\n");

        return $this;
    }

    protected function writeLabelTo(WriterInterface $writer)
    {
        if ($this->label) {
            $writer->writeFormatted(' : %s', $this->label);
        }

        return $this;
    }

    protected function writeLinkTo(WriterInterface $writer, $linkSource = null, $linkTarget = null)
    {
        $this->writeSourceCardinalityTo($writer);
        $writer->writeFormatted(' %s%s%s ', $linkSource ?: $this->linkSource, $this->link, $linkTarget ?: $this->linkTarget);
        $this->writeTargetCardinalityTo($writer);

        return $this;
    }

    protected function writeSourceCardinalityTo(WriterInterface $writer)
    {
        if ($this->sourceCardinality !== '') {
            $writer->writeFormatted(' "%s"', $this->sourceCardinality);
        }

        return $this;
    }

    protected function writeTargetCardinalityTo(WriterInterface $writer)
    {
        if ($this->targetCardinality !== '') {
            $writer->writeFormatted('"%s" ', $this->targetCardinality);
        }

        return $this;
    }
}
