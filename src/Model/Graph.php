<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model;

use Irstea\PlantUmlBundle\Finder\FinderInterface;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Graph.
 */
class Graph implements GraphInterface
{
    /**
     * @var ClassVisitorInterface
     */
    private $visitor;

    /**
     * @var FinderInterface
     */
    private $finder;

    /**
     * Graph constructor.
     */
    public function __construct(ClassVisitorInterface $visitor, FinderInterface $finder)
    {
        $this->visitor = $visitor;
        $this->finder = $finder;
    }

    public function visitAll(): void
    {
        foreach ($this->finder->getIterator() as $class) {
            $this->visitor->visitClass($class);
        }
    }

    /**
     * @return $this
     */
    public function writeTo(WriterInterface $writer): self
    {
        $writer->write("@startuml@\n");
        $this->visitor->writeTo($writer);
        $writer->write("@enduml@\n");

        return $this;
    }

    public function toConfig(array &$conf): void
    {
        $conf['sources'] = [];
        $this->finder->toConfig($conf['sources']);
        $this->visitor->toConfig($conf);
    }
}
