<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Namespace_;

use Irstea\PlantUmlBundle\Model\NamespaceInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use Irstea\PlantUmlBundle\Writer\WritableInterface;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Namespace.
 */
abstract class AbstractNamespace implements WritableInterface, NamespaceInterface
{
    /**
     * string.
     */
    protected const CONF_TYPE = 'UNDEFINED';

    /**
     * @var NodeInterface[]
     */
    private $nodes = [];

    public function addNode(NodeInterface $node)
    {
        $this->nodes[] = $node;

        return $this;
    }

    /**
     * @return self
     */
    protected function writeNodesTo(WriterInterface $writer)
    {
        foreach ($this->nodes as $node) {
            $node->writeTo($writer);
        }

        return $this;
    }

    /**
     * @return bool
     */
    protected function isEmpty()
    {
        return empty($this->nodes);
    }

    public function toConfig(array &$conf): void
    {
        $conf['namespace'] = static::CONF_TYPE;
    }
}
