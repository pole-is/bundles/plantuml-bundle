<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Namespace_;

/**
 * Description of BundleNamespace.
 */
class BundleNamespace extends MappedNamespace
{
    public const CONF_TYPE = 'bundles';
    public const SEPARATOR = '::';

    public function __construct(array $bundles)
    {
        $mapping = [];
        foreach ($bundles as $bundle => $php) {
            $mapping[substr($php, 0, 1 + strrpos($php, '\\'))] = $bundle . '::';
        }

        parent::__construct($mapping);
    }
}
