<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Namespace_\Php;

use Irstea\PlantUmlBundle\Model\Namespace_\AbstractNamespace as Base;
use Irstea\PlantUmlBundle\Model\NamespaceInterface;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Namespace.
 */
abstract class AbstractNamespace extends Base
{
    /**
     * @var NamespaceInterface
     */
    private $children = [];

    /**
     * @param string $namespaceName
     *
     * @return NamespaceInterface
     */
    public function getNamespace($namespaceName)
    {
        $namespaceName = trim($namespaceName, '\\');
        if (!$namespaceName) {
            return $this;
        }
        @list($head, $tail) = explode('\\', $namespaceName, 2);
        if (!isset($this->children[$head])) {
            $this->children[$head] = new Namespace_($this, $head);
        }
        if (empty($tail)) {
            return $this->children[$head];
        }

        return $this->children[$head]->getNamespace($tail);
    }

    /**
     * @return $string
     */
    abstract protected function getNamespacePrefix();

    public function getNodeId($name)
    {
        return str_replace('\\', '.', $name) . '_node';
    }

    /**
     * @return self
     */
    protected function writeChildrenTo(WriterInterface $writer)
    {
        foreach ($this->children as $child) {
            $child->writeTo($writer);
        }

        return $this;
    }

    /**
     * @return bool
     */
    protected function isEmpty()
    {
        return parent::isEmpty() && empty($this->children);
    }
}
