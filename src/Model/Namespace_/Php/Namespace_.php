<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Namespace_\Php;

use Irstea\PlantUmlBundle\Model\ArrowInterface;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Namespace.
 *
 * @SuppressWarnings("PHPMD.CamelCaseClassName")
 */
class Namespace_ extends AbstractNamespace
{
    /**
     * @var AbstractNamespace
     */
    private $parent;

    /**
     * @var string
     */
    private $name;

    public function __construct(AbstractNamespace $parent, $name)
    {
        $this->parent = $parent;
        $this->name = $name;
    }

    /**
     * @return self
     */
    public function addArrow(ArrowInterface $arrow)
    {
        $this->parent->addArrow($arrow);

        return $this;
    }

    /**
     * @return string
     */
    protected function getNamespacePrefix()
    {
        return $this->parent->getNamespacePrefix() . $this->name . '\\';
    }

    public function getNodeLabel($name)
    {
        $prefix = $this->getNamespacePrefix();
        if (0 === strpos($name, $prefix)) {
            return substr($name, \strlen($prefix));
        }

        return $name;
    }

    /**
     * @param resource WriterInterface $writer
     *
     * @return self
     */
    public function writeTo(WriterInterface $writer)
    {
        if ($this->isEmpty()) {
            return;
        }

        $writer
            ->writeFormatted("namespace %s {\n", $this->name)
            ->indent();
        $this
            ->writeNodesTo($writer)
            ->writeChildrenTo($writer);
        $writer
            ->dedent()
            ->write("}\n");

        return $this;
    }
}
