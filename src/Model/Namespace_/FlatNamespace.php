<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Namespace_;

use Irstea\PlantUmlBundle\Model\ArrowInterface;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of RootNamespace.
 */
class FlatNamespace extends AbstractNamespace
{
    public const CONF_TYPE = 'flat';
    public const SEPARATOR = 'none';

    /**
     * @var ArrowInterface[]
     */
    private $arrows = [];

    public function addArrow(ArrowInterface $arrow)
    {
        $this->arrows[] = $arrow;

        return $this;
    }

    public function writeTo(WriterInterface $writer)
    {
        $writer->writeFormatted("set namespaceSeparator %s\n", static::SEPARATOR);
        $this
            ->writeNodesTo($writer)
            ->writeArrowsTo($writer);

        return $this;
    }

    protected function writeArrowsTo(WriterInterface $writer)
    {
        foreach ($this->arrows as $arrow) {
            $arrow->writeTo($writer);
        }

        return $this;
    }

    public function getNamespace($namespaceName)
    {
        return $this;
    }

    public function getNodeId($name)
    {
        return str_replace('\\', '_', $name);
    }

    public function getNodeLabel($name)
    {
        return $name;
    }
}
