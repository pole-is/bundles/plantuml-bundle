<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Decorator;

use Irstea\PlantUmlBundle\Model\ClassVisitorInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Décorateur abstrait pour les éléments d'une classe qui peuvent être hérités d'une classe base.
 * Ce décorateur s'assure de n'afficher que les éléments propres à la classe.
 */
trait InheritableItemDecoratorTrait
{
    /**
     * @return self
     */
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor)
    {
        $items = $this->extractItems($class);
        if (empty($items)) {
            return;
        }

        $parent = $class->getParentClass();
        if ($parent) {
            $parentItems = $this->extractItems($parent);
            if (!empty($parentItems)) {
                $items = array_diff_key($items, $parentItems);
            }
        }

        foreach ($items as $item) {
            $this->decorateItem($class, $node, $visitor, $item);
        }
    }

    abstract protected function extractItems(ReflectionClass $class);

    abstract protected function decorateItem(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor, $item);
}
