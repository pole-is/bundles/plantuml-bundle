<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Decorator;

use Irstea\PlantUmlBundle\Model\Arrow\ImplementsInterface;
use Irstea\PlantUmlBundle\Model\ClassVisitorInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;

/**
 * Description of InheritanceDecorator.
 */
class InterfaceDecorator extends AbstractRelationDecorator
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor)
    {
        $interfaces = $class->getInterfaceNames();

        $indirectInterfaces = array_filter(
            array_map(
                function ($i) {
                    return $i->getInterfaceNames();
                },
                $class->getInterfaces()
            )
        );

        if (!empty($indirectInterfaces)) {
            $indirectInterfaces = \call_user_func_array('array_merge', $indirectInterfaces);
            $interfaces = array_diff($interfaces, $indirectInterfaces);
        }

        $parent = $class->getParentClass();
        if ($parent) {
            $interfaces = array_diff($interfaces, $parent->getInterfaceNames());
        }

        $this->visitRelations($node, $interfaces, $visitor);

        return $this;
    }

    protected function buildRelation(NodeInterface $source, NodeInterface $target)
    {
        return new ImplementsInterface($source, $target);
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'interfaces';
    }
}
