<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Decorator;

use Irstea\PlantUmlBundle\Model\ClassVisitorInterface;
use Irstea\PlantUmlBundle\Model\DecoratorInterface;
use Irstea\PlantUmlBundle\Model\Node\Member\Member;
use Irstea\PlantUmlBundle\Model\Node\Member\MemberInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;
use ReflectionMethod;

/**
 * Description of AttributeDecorator.
 */
class MethodDecorator implements DecoratorInterface
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        foreach ($class->getMethods() as $method) {
            /* @var $method ReflectionMethod */

            if ($method->getDeclaringClass() != $class || $this->isAccessor($method, $class)) {
                continue;
            }

            $node->addMethod(
                new Member(
                    $method->getName() . '(...)',
                    '',
                    $method->isPrivate() ? MemberInterface::PRIVATE_ :
                        ($method->isProtected() ? MemberInterface::PROTECTED_ :
                            ($method->isPublic() ? MemberInterface::PUBLIC_ :
                                MemberInterface::UNKNOWN))
                )
            );
        }
    }

    /**
     * @return bool
     */
    protected function isAccessor(ReflectionMethod $method, ReflectionClass $class)
    {
        if (!$method->isPublic() || $method->isAbstract() || $method->getDeclaringClass()->isInterface()) {
            return false;
        }
        if ($method->getNumberOfParameters() === 0 && preg_match('/(?:get|is)(\w+)/', $method->getName(), $groups)) {
            $name = lcfirst($groups[1]);
        } elseif ($method->getNumberOfParameters() === 1 && preg_match('/(?:set|add|remove)(\w+)/', $method->getName(), $groups)) {
            $name = lcfirst($groups[1]);
        } else {
            return false;
        }
        if (!$class->hasProperty($name)) {
            return false;
        }
        $property = $class->getProperty($name);

        return $property->isStatic() == $method->isStatic();
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'methods';
    }
}
