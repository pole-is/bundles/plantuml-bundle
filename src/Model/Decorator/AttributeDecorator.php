<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Decorator;

use Irstea\PlantUmlBundle\Model\ClassVisitorInterface;
use Irstea\PlantUmlBundle\Model\DecoratorInterface;
use Irstea\PlantUmlBundle\Model\Node\Member\Member;
use Irstea\PlantUmlBundle\Model\Node\Member\MemberInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use ReflectionClass;
use ReflectionProperty;

/**
 * Description of AttributeDecorator.
 */
class AttributeDecorator implements DecoratorInterface
{
    public function decorate(ReflectionClass $class, NodeInterface $node, ClassVisitorInterface $visitor): void
    {
        foreach ($class->getProperties() as $property) {
            /* @var $property ReflectionProperty */

            if ($property->getDeclaringClass() != $class) {
                continue;
            }

            $node->addAttribute(
                new Member(
                    $property->getName(),
                    false,
                    $property->isPrivate() ? MemberInterface::PRIVATE_ :
                        $property->isProtected() ? MemberInterface::PROTECTED_ :
                            $property->isPublic() ? MemberInterface::PUBLIC_ :
                                MemberInterface::UNKNOWN
                )
            );
        }
    }

    public function toConfig(array &$conf): void
    {
        $conf['decorators'][] = 'attributes';
    }
}
