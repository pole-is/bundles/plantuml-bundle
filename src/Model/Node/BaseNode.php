<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Node;

use Irstea\PlantUmlBundle\Model\ArrowInterface;
use Irstea\PlantUmlBundle\Model\NamespaceInterface;
use Irstea\PlantUmlBundle\Model\Node\Member\AttributeInterface;
use Irstea\PlantUmlBundle\Model\Node\Member\MethodInterface;
use Irstea\PlantUmlBundle\Model\NodeInterface;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of Class.
 */
class BaseNode implements NodeInterface
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $nodeType;

    /**
     * @var string[]
     */
    private $classifiers;

    /**
     * @var string[]
     */
    private $stereotypes;

    /**
     * @var NamespaceInterface
     */
    private $namespace;

    /**
     * @var AttributeInterface[]
     */
    private $attributes = [];

    /**
     * @var MethodInterface[]
     */
    private $methods = [];

    /**
     * @param string $name
     * @param string $nodeType
     */
    public function __construct(NamespaceInterface $namespace, $name, $nodeType, array $classifiers = [], array $stereotypes = [])
    {
        $this->namespace = $namespace;
        $this->id = $namespace->getNodeId($name);
        $this->label = $namespace->getNodeLabel($name);

        $this->nodeType = $nodeType;
        $this->classifiers = $classifiers;
        $this->stereotypes = $stereotypes;
    }

    public function addClassifier($classifier)
    {
        $this->classifiers[] = $classifier;

        return $this;
    }

    public function addStereotype($stereotype)
    {
        $this->stereotypes[] = $stereotype;

        return $this;
    }

    public function addAttribute(AttributeInterface $attribute)
    {
        $this->attributes[] = $attribute;

        return $this;
    }

    public function addMethod(MethodInterface $method)
    {
        $this->methods[] = $method;

        return $this;
    }

    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    public function writeTo(WriterInterface $writer)
    {
        $this
            ->writeClassifiersTo($writer)
            ->writeNodeTypeTo($writer);
        $writer->writeFormatted('"%s" as %s', $this->label, $this->id);
        $this
            ->writeStereotypesTo($writer);
        $writer
            ->write(" {\n")
            ->indent();
        $this
            ->writeAttributesTo($writer)
            ->writeMethodsTo($writer);
        $writer
            ->dedent()
            ->write("}\n");

        return $this;
    }

    public function writeAliasTo(WriterInterface $writer)
    {
        $writer->write($this->id);

        return $this;
    }

    protected function writeClassifiersTo(WriterInterface $writer)
    {
        foreach ($this->classifiers as $classifier) {
            $writer->writeFormatted('%s ', $classifier);
        }

        return $this;
    }

    protected function writeNodeTypeTo(WriterInterface $writer)
    {
        $writer->writeFormatted('%s ', $this->nodeType);

        return $this;
    }

    protected function writeStereotypesTo(WriterInterface $writer)
    {
        foreach ($this->stereotypes as $stereotype) {
            $writer->writeFormatted(' <<%s>>', $stereotype);
        }

        return $this;
    }

    protected function writeAttributesTo(WriterInterface $writer)
    {
        foreach ($this->attributes as $attribute) {
            $attribute->writeTo($writer);
        }

        return $this;
    }

    protected function writeMethodsTo(WriterInterface $writer)
    {
        foreach ($this->methods as $method) {
            $method->writeTo($writer);
        }

        return $this;
    }

    public function addArrow(ArrowInterface $arrow)
    {
        $this->namespace->addArrow($arrow);

        return $this;
    }
}
