<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model\Node\Member;

use Irstea\PlantUmlBundle\Model\TypedSymbol;
use Irstea\PlantUmlBundle\Writer\WriterInterface;

/**
 * Description of AbstractMember.
 */
class Member extends TypedSymbol implements MethodInterface, AttributeInterface
{
    /**
     * @var string
     */
    private $visibility;

    /**
     * @param string      $symbol
     * @param string|bool $type
     * @param string      $visibility
     */
    public function __construct($symbol, $type = false, $visibility = self::UNKNOWN)
    {
        parent::__construct($symbol, $type);
        $this->visibility = $visibility;
    }

    public function writeTo(WriterInterface $writer)
    {
        $writer->write($this->visibility);
        $this->writeMemberTo($writer);
        $writer->write("\n");

        return $this;
    }

    protected function writeMemberTo(WriterInterface $writer)
    {
        return parent::writeTo($writer);
    }
}
