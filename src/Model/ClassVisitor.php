<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\Model;

use Irstea\PlantUmlBundle\Model\Decorator\NullDecorator;
use Irstea\PlantUmlBundle\Model\Filter\AcceptAllFilter;
use Irstea\PlantUmlBundle\Model\Namespace_\Php\RootNamespace;
use Irstea\PlantUmlBundle\Model\Node\Class_;
use Irstea\PlantUmlBundle\Model\Node\Interface_;
use Irstea\PlantUmlBundle\Model\Node\Trait_;
use Irstea\PlantUmlBundle\Writer\WriterInterface;
use ReflectionClass;

/**
 * Description of Visitor.
 */
class ClassVisitor implements ClassVisitorInterface
{
    /**
     * @var NamespaceInterface
     */
    protected $rootNamespace;

    /**
     * @var ClassFilterInterface
     */
    protected $filter;

    /**
     * @var DecoratorInterface
     */
    protected $decorator;

    /**
     * @var array<string, NodeInterface>
     */
    private $nodes;

    public function __construct(
        DecoratorInterface $decorator = null,
        ClassFilterInterface $filter = null,
        NamespaceInterface $namespace = null
    ) {
        $this->filter = $filter ?: AcceptAllFilter::instance();
        $this->decorator = $decorator ?: NullDecorator::instance();
        $this->rootNamespace = $namespace ?: new RootNamespace();
    }

    /**
     * @return self
     */
    public function setClassFilter(ClassFilterInterface $filter)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @return self
     */
    public function setDecorator(DecoratorInterface $decorator)
    {
        $this->decorator = $decorator;

        return $this;
    }

    public function visitClass($classOrName)
    {
        if ($classOrName instanceof \ReflectionClass) {
            $class = $classOrName;
        } elseif (\is_string($classOrName)) {
            $class = new \ReflectionClass($classOrName);
        } else {
            throw new \InvalidArgumentException('Invalid argument, expected ReflectionClass or string');
        }
        $className = $class->getName();

        if (isset($this->nodes[$className])) {
            return $this->nodes[$className];
        }

        if (!$this->filter->accept($class)) {
            return $this->nodes[$className] = false;
        }

        $namespace = $this->rootNamespace->getNamespace($class->getNamespaceName());
        $node = $this->nodes[$className] = $this->createNode($namespace, $class);
        $namespace->addNode($node);

        $this->decorator->decorate($class, $node, $this);

        return $node;
    }

    /**
     * @return NodeInterface
     */
    protected function createNode(NamespaceInterface $namespace, ReflectionClass $class)
    {
        $className = $class->getName();

        if ($class->isTrait()) {
            return new Trait_($namespace, $className);
        }

        if ($class->isInterface()) {
            return new Interface_($namespace, $className);
        }

        return new Class_($namespace, $className, $class->isAbstract(), $class->isFinal());
    }

    public function writeTo(WriterInterface $writer)
    {
        return $this->rootNamespace->writeTo($writer);
    }

    public function toConfig(array &$conf): void
    {
        $conf['layout'] = [];
        $conf['decoration'] = [];

        $this->filter->toConfig($conf['layout']);
        $this->rootNamespace->toConfig($conf['layout']);
        $this->decorator->toConfig($conf['decoration']);
    }
}
