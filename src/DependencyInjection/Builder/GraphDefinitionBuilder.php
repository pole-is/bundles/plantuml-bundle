<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\DependencyInjection\Builder;

use Irstea\PlantUmlBundle\Doctrine\EntityFinder;
use Irstea\PlantUmlBundle\Finder\ClassFinder;
use Irstea\PlantUmlBundle\Finder\FilteringFinder;
use Irstea\PlantUmlBundle\Model\ClassVisitor;
use Irstea\PlantUmlBundle\Model\Graph;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Description of GraphDefinitionBuilder.
 */
class GraphDefinitionBuilder
{
    /**
     * @var ContainerBuilder
     */
    private $container;

    /**
     * @var string
     */
    private $baseId;

    /**
     * @var array
     */
    private $config;

    /**
     * @var ClassFilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Reference
     */
    private $entityManager;

    /**
     * @var Definition
     */
    private $definition;

    /**
     * @param string $baseId
     */
    public function __construct(ContainerBuilder $container, $baseId, array $config, ClassFilterBuilder $filterBuilder)
    {
        $this->container = $container;
        $this->baseId = $baseId;
        $this->config = $config;
        $this->filterBuilder = $filterBuilder;

        $emId = $config['sources']['entity_manager'];
        $this->entityManager = new Reference("doctrine.orm.${emId}_entity_manager");
    }

    public function build(): Definition
    {
        if (!$this->definition) {
            return $this->definition = $this->doBuild();
        }

        return $this->definition;
    }

    protected function doBuild(): Definition
    {
        [$source, $sourceFilter] = $this->buildSources();

        $layoutFilter = $this->filterBuilder->build($this->config['layout']) ?: $sourceFilter;
        $decorator = $this->buildFilteredDecorator();
        $namespace = $this->buildNamespace();

        $visitor = $this->setDefinition('visitor', ClassVisitor::class, $decorator, $layoutFilter, $namespace);

        $def = new Definition(Graph::class, [$visitor, $source]);
        $def->setLazy(true);
        $def->setPublic(true);

        return $def;
    }

    /**
     * @return Reference[]
     */
    protected function buildSources(): array
    {
        $finder = $this->buildFinder();
        $filter = $this->filterBuilder->build($this->config['sources']);

        if ($filter) {
            $filtered = $this->setDefinition('finder', FilteringFinder::class, $finder, $filter);

            return [$filtered, $filter];
        }

        return [$finder, null];
    }

    protected function buildFinder(): Reference
    {
        $config = $this->config['sources'];

        switch ($config['type']) {
            case 'entities':
                return $this->setDefinition('finder.entities', EntityFinder::class, $this->entityManager);
            case 'classes':
                return $this->setDefinition('finder.classes', ClassFinder::class, $config['directories']);
        }

        throw new \InvalidArgumentException("Invalid source type: {$config['type']}");
    }

    protected function buildFilteredDecorator(): Reference
    {
        $decorator = $this->buildDecorator();
        if (!$decorator) {
            return $decorator;
        }

        $filter = $this->filterBuilder->build($this->config['decoration']);
        if (!$filter) {
            return $decorator;
        }

        return $this->setDefinitionDecorator('decorator', 'irstea.plant_uml.decorator.filtered.template', $decorator, $filter);
    }

    protected function buildDecorator(): Reference
    {
        $config = $this->config['decoration']['decorators'];

        if (empty($config)) {
            return null;
        }

        if (\count($config) === 1) {
            return $this->buildTypedDecorator($config[0]);
        }

        $decorators = [];
        foreach ($config as $type) {
            $decorators[] = $this->buildTypedDecorator($type);
        }

        return $this->setDefinitionDecorator(
            'decorator.all',
            'irstea.plant_uml.decorator.composite.template',
            $decorators
        );
    }

    /**
     * @param string $type
     */
    protected function buildTypedDecorator($type): Reference
    {
        if (\in_array($type, ['entity', 'associations', 'fields'], true)) {
            return $this->setDefinitionDecorator(
                "decorator.$type",
                "irstea.plant_uml.decorator.$type.template",
                $this->entityManager
            );
        }

        return new Reference("irstea.plant_uml.decorator.$type");
    }

    protected function buildNamespace(): Reference
    {
        $type = $this->config['layout']['namespaces'];

        if ($type === 'entities') {
            return $this->setDefinitionDecorator(
                "namespace.$type",
                "irstea.plant_uml.namespaces.$type.template",
                $this->entityManager
            );
        }

        return $this->setDefinitionDecorator(
            "namespace.$type",
            "irstea.plant_uml.namespaces.$type.template"
        );
    }

    /**
     * @param string            $localId
     * @param string|Definition $classOrDef
     * @param array             ...$arguments
     */
    protected function setDefinition($localId, $classOrDef, ...$arguments): Reference
    {
        if ($classOrDef instanceof Definition) {
            $definition = $classOrDef;
        } else {
            $definition = new Definition($classOrDef, $arguments);
        }
        $id = $this->globalId($localId);
        $this->container->setDefinition($id, $definition);

        return new Reference($id);
    }

    /**
     * @param string $localId
     * @param string $templateId
     * @param array  ...$arguments
     */
    protected function setDefinitionDecorator($localId, $templateId, ...$arguments): Reference
    {
        $def = new ChildDefinition($templateId);
        $def->setArguments($arguments);

        return $this->setDefinition($localId, $def);
    }

    /**
     * @param string $localId
     */
    protected function globalId($localId): string
    {
        return "{$this->baseId}.$localId";
    }
}
