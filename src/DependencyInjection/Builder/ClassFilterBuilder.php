<?php declare(strict_types=1);
/*
 * This file is part of "irstea/plantuml-bundle".
 *
 * Copyright (C) 2016-2020 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\PlantUmlBundle\DependencyInjection\Builder;

use Irstea\PlantUmlBundle\Model\Filter\ClassFilter;
use Irstea\PlantUmlBundle\Model\Filter\Composite\AllFilter;
use Irstea\PlantUmlBundle\Model\Filter\DirectoryFilter;
use Irstea\PlantUmlBundle\Model\Filter\NamespaceFilter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Description of ClassFilterBuilder.
 */
class ClassFilterBuilder
{
    /**
     * @var ContainerBuilder
     */
    private $container;

    /**
     * @var Reference[]
     */
    private $filters = [];

    /**
     * @var string[]
     */
    private static $filterClasses = [
        'classes'     => ClassFilter::class,
        'directories' => DirectoryFilter::class,
        'namespaces'  => NamespaceFilter::class,
    ];

    /**
     * @param \Irstea\PlantUmlBundle\DependencyInjection\Builder\ContainerBuilder $container
     */
    public function __construct(ContainerBuilder $container)
    {
        $this->container = $container;
    }

    /**
     * @return Reference
     */
    public function build(array $config)
    {
        $filters = $this->normalize(array_intersect_key($config, ['include' => true, 'exclude' => true]));
        if ($filters === null) {
            return null;
        }

        $hash = sha1(serialize($filters));
        if (!\array_key_exists($hash, $this->filters)) {
            return $this->filters[$hash] = $this->doBuild("irstea.plant_uml.filters.$hash", $filters);
        }

        return $this->filters[$hash];
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    protected function normalize($data)
    {
        if ($data === null || $data === []) {
            return null;
        }
        if (\is_array($data)) {
            $res = [];
            $isList = true;
            foreach ($data as $k => $v) {
                $normalized = $this->normalize($v);
                if (!empty($normalized)) {
                    $res[$k] = $normalized;
                    if (!\is_int($k)) {
                        $isList = false;
                    }
                }
            }
            if (empty($res)) {
                return null;
            }
            if ($isList) {
                sort($res);
            } else {
                ksort($res);
            }

            return $res;
        }

        return $data;
    }

    /**
     * @param string $id
     *
     * @return Reference
     */
    protected function doBuild($id, array $config)
    {
        $filters = [];

        if (isset($config['include'])) {
            $this->buildSubFilter($filters, "$id.include", $config['include'], false);
        }
        if (isset($config['exclude'])) {
            $this->buildSubFilter($filters, "$id.exclude", $config['exclude'], true);
        }

        if (empty($filters)) {
            return null;
        }

        if (\count($filters) === 1) {
            return $filters[0];
        }

        $this->container->setDefinition($id, new Definition(AllFilter::class, [$filters]));

        return new Reference($id);
    }

    /**
     * @param string $id
     * @param bool   $notFound
     */
    protected function buildSubFilter(array &$filters, $id, array $config, $notFound): void
    {
        foreach (self::$filterClasses as $key => $class) {
            if (!isset($config[$key])) {
                continue;
            }
            $subId = "$id.$key";
            $def = new Definition($class, [$config[$key], $notFound]);
            $this->container->setDefinition($subId, $def);
            $filters[] = new Reference($subId);
        }
    }
}
